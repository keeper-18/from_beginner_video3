let d = document,
    age = d.getElementById('age'),
    result = d.getElementById('result'),
    btn = d.getElementById('btn'),
    computedType = d.getElementById('computedType'),
    btnGetType = d.getElementById('btnGetType');

function inputHandler() {
    let val = age.value ? age.value : 'empty';
    val = Number(val);
    result.innerText = val + ' ' + typeof(val);
}

function showType() {
    let res = null == 1;
    computedType.innerText = res + ' ' + typeof(res);
}

btn.addEventListener('click', inputHandler);

btnGetType.addEventListener('click', showType);